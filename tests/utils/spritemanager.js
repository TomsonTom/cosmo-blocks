/*
 * Sprite Manager Test
 * utils/spritemanager
 */

define(["util/spritemanager"], function(SpriteManager)
{
    describe("util/spritemanager", function()
    {
        // A sprite list for testing purposes (not REAL paths)
        var sprites =
        {
            "spritesheet_a": // Sprite with constant tile width, height
            {
                path: "img/image_a.png",
                tile_width: 120,
                tile_height: 160,
                padding_x: 5,
                padding_y: 2,
                elements:
                {
                    "a_1": [0, 0],
                    "a_2": [1, 0],
                    "a_3": [0, 1],
                    "a_4": [1, 1]
                }
            },

            "spritesheet_b": // Sprite with variable element sizes
            {
                path: "img/image_b.png",
                elements:
                {
                    //      x    y  w    h
                    "b_1": [0,   0, 120, 200],
                    "b_2": [120, 0, 150, 150]
                }
            },

            "singlesprite": // Image file with a single sprite
            {
                path: "img/singlesprite.png"
            }
        };

        var sprmanager = new SpriteManager(sprites);

        it(".GetPaths() returns the paths of all defined sprites", function()
        {
            var paths = sprmanager.GetPaths();

            expect(paths).toContain(sprites["spritesheet_a"].path);
            expect(paths).toContain(sprites["spritesheet_b"].path);
            expect(paths).toContain(sprites["singlesprite"].path);
        });

        it(".Load() forces Crafty to preload all defined sprites", function()
        {
            spyOn(Crafty, "load");

            sprmanager.Load();

            // The paths of all defined images
            var paths = [sprites["spritesheet_a"].path, sprites["spritesheet_b"].path, sprites["singlesprite"].path];

            expect(Crafty.load.mostRecentCall.args[0]).toEqual(paths);
        });

        describe(".Create() [called after .Load()]", function()
        {
            beforeEach(function()
            {
                // We must fake the Crafty.asset function, so it looks like the sprites have been already loaded using .Load()
                // We also set the width and height of the sprites manually, because the paths aren't real
                spyOn(Crafty, "asset").andCallFake(function(key)
                {
                    var img = {};
                    img.src = key;
                    img.width = 120;
                    img.height = 240;
                    return img;
                });

                spyOn(Crafty, "sprite");

                sprmanager.Create();
            });

            it("forces Crafty to create defined sprites - with constant tile width, height", function()
            {
                // spritesheet_a - with constant tile width, height
                var spr = sprites.spritesheet_a;
                expect(Crafty.sprite).toHaveBeenCalledWith(spr.tile_width, spr.tile_height,
                    spr.path, spr.elements, spr.padding_x, spr.padding_y);
            });

            it("forces Crafty to create defined sprites - with variable element sizes", function()
            {
                // spritesheet_b - with variable element sizes
                var spr = sprites.spritesheet_b;
                expect(Crafty.sprite).toHaveBeenCalledWith(undefined, undefined, spr.path, spr.elements, undefined, undefined);
            });

            it("forces Crafty to create defined sprites - single sprite without defined size", function()
            {
                // singlesprite - without defined size
                // the size will have to be determined from Image object
                var spr = sprites.singlesprite;
                expect(Crafty.sprite).toHaveBeenCalledWith(spr.path, {"singlesprite": [0, 0, 120, 240]});
            });
        });

        describe(".Create() [before loading the sprites]", function()
        {
            beforeEach(function()
            {
                spyOn(Crafty, "sprite");
                spyOn(Crafty, "load");
                sprmanager.Create();
            });

            it("forces Crafty to load the missing sprites first", function()
            {
                expect(Crafty.load).toHaveBeenCalledWith(sprites.spritesheet_a.path);
                expect(Crafty.load).toHaveBeenCalledWith(sprites.spritesheet_b.path);
                expect(Crafty.load).toHaveBeenCalledWith(sprites.singlesprite.path);
            });
        });
    });
});