/*
 * Combination Checker Unit Test
 * utils/combinationchecker
 */

define(["util/combinationchecker", "util/craftyfinder"], function()
{
    describe("util/combinationchecker", function()
    {
        beforeEach(function()
        {
            // Destroy all entities before running each subtest
            var entities = CraftyFinder.Find("block");
            _.forEach(entities, function(ent)
            {
                ent.destroy();
            });
        });

        afterEach(function()
        {
            // Destroy all entities after running each subtest
            var entities = CraftyFinder.Find("block");
            _.forEach(entities, function(ent)
            {
                ent.destroy();
            });
        });

        describe("CombinationChecker.FindNear(block, whereToSearch)", function()
        {
            it ("finds all blocks around a block", function()
            {
                var blocks = [];
                var centerBlock = Crafty.e("2D, block, block_red").attr({x:70,y:70});
                blocks.push(centerBlock);
                blocks.push(Crafty.e("2D, block, block_red").attr({x:70,y:0,blockColour:"red"}));
                blocks.push(Crafty.e("2D, block, block_red").attr({x:70,y:140,blockColour:"red"}));
                blocks.push(Crafty.e("2D, block, block_red").attr({x:0,y:70,blockColour:"red"}));
                blocks.push(Crafty.e("2D, block, block_red").attr({x:140,y:70,blockColour:"red"}));
                blocks.push(Crafty.e("2D, block, block_red").attr({x:140,y:210,blockColour:"red"}));

                expect(CombinationChecker.FindNear(centerBlock, blocks).length).toEqual(4);
            });
        });

        describe("CombinationChecker.GetGroups() returns array of arrays of blocks that touches and have the same colour", function()
        {
            it("4 red blocks placed horizontally", function()
            {
                var blocks = [];

                for (var i = 0; i < 4; i++)
                {
                    var block = Crafty.e("2D, block, block_red");
                    block.attr({x:i*70,y:530-0,blockColour:"red"});
                    blocks.push(block);
                }

                var groups = CombinationChecker.GetGroups();
                expect(groups.length).toEqual(1);

                _.forEach(blocks, function(block)
                {
                    expect(groups[0]).toContain(block);
                });
            });

            it("4 yellow blocks placed vertically", function()
            {
                var blocks = [];

                for (var i = 0; i < 4; i++)
                {
                    blocks.push(Crafty.e("2D, block, block_yellow").attr({x:0,y:530-i*70,blockColour:"yellow"}));
                }

                expect(CombinationChecker.GetGroups().length).toEqual(1);

                _.forEach(blocks, function(block)
                {
                    expect(CombinationChecker.GetGroups()[0]).toContain(block);
                });
            });

            it("4 blue blocks placed in square", function()
            {
                var blocks = [];

                blocks.push(Crafty.e("2D, block, block_blue").attr({x:0,y:530-0,blockColour:"blue"}));
                blocks.push(Crafty.e("2D, block, block_blue").attr({x:0,y:530-70,blockColour:"blue"}));
                blocks.push(Crafty.e("2D, block, block_blue").attr({x:70,y:530-0,blockColour:"blue"}));
                blocks.push(Crafty.e("2D, block, block_blue").attr({x:70,y:530-70,blockColour:"blue"}));

                expect(CombinationChecker.GetGroups().length).toEqual(1);

                _.forEach(blocks, function(block)
                {
                    expect(CombinationChecker.GetGroups()[0]).toContain(block);
                });
            });

            it("4 green blocks placed in L", function()
            {
                var blocks = [];

                blocks.push(Crafty.e("2D, block, block_green").attr({x:0,y:530-0,blockColour:"green"}));
                blocks.push(Crafty.e("2D, block, block_green").attr({x:0,y:530-70,blockColour:"green"}));
                blocks.push(Crafty.e("2D, block, block_green").attr({x:0,y:530-140,blockColour:"green"}));
                blocks.push(Crafty.e("2D, block, block_green").attr({x:70,y:530-140,blockColour:"green"}));

                expect(CombinationChecker.GetGroups().length).toEqual(1);

                _.forEach(blocks, function(block)
                {
                    expect(CombinationChecker.GetGroups()[0]).toContain(block);
                });
            });

            it("8 blue blocks placed in E", function()
            {
                var blocks = [];

                blocks.push(Crafty.e("2D, block, block_blue").attr({x:0,y:530-0,blockColour:"blue"}));
                blocks.push(Crafty.e("2D, block, block_blue").attr({x:70,y:530-0,blockColour:"blue"}));
                blocks.push(Crafty.e("2D, block, block_blue").attr({x:0,y:530-70,blockColour:"blue"}));
                blocks.push(Crafty.e("2D, block, block_blue").attr({x:0,y:530-140,blockColour:"blue"}));
                blocks.push(Crafty.e("2D, block, block_blue").attr({x:70,y:530-140,blockColour:"blue"}));
                blocks.push(Crafty.e("2D, block, block_blue").attr({x:0,y:530-210,blockColour:"blue"}));
                blocks.push(Crafty.e("2D, block, block_blue").attr({x:0,y:530-280,blockColour:"blue"}));
                blocks.push(Crafty.e("2D, block, block_blue").attr({x:70,y:530-280,blockColour:"blue"}));

                expect(CombinationChecker.GetGroups().length).toEqual(1);

                _.forEach(blocks, function(block)
                {
                    expect(CombinationChecker.GetGroups()[0]).toContain(block);
                });
            });

            it("ignore blocks that are falling", function()
            {
                var blocks = [];

                blocks.push(Crafty.e("2D, block, block_blue").attr({x:70,y:530-0,_falling:true,blockColour:"blue"})); // This one is falling

                expect(CombinationChecker.GetGroups().length).toEqual(0);
            });

            it("ignore blocks that aren't in a grid (i.e. are being pushed)", function()
            {
                var blocks = [];

                blocks.push(Crafty.e("2D, block, block_blue").attr({x:50,y:530-0,blockColour:"blue"})); // This one is not in a grid

                expect(CombinationChecker.GetGroups().length).toEqual(0);
            });
        });

        describe("CombinationChecker.Check()", function()
        {
            var groups = [];
            var blocksToRemove1 = [];
            var blocksToRemove2 = [];
            var blocksNotToRemove = [];

            PlayerData = {};
            PlayerData.score = 0;

            beforeEach(function()
            {
            });

            afterEach(function()
            {
                groups.length = 0;
                blocksToRemove1.length = 0;
                blocksToRemove2.length = 0;
                blocksNotToRemove.length = 0;

                PlayerData.score = 0;
            });

            it("removes all groups of >3 blocks returned by .GetGroups()", function()
            {
                spyOn(CombinationChecker, "GetGroups").andCallFake(function()
                {
                    blocksToRemove1.push(Crafty.e("2D, block, block_blue").attr({x:0,y:530-0,blockColour:"blue"}));
                    blocksToRemove1.push(Crafty.e("2D, block, block_blue").attr({x:70,y:530-0,blockColour:"blue"}));
                    blocksToRemove1.push(Crafty.e("2D, block, block_blue").attr({x:0,y:530-70,blockColour:"blue"}));
                    blocksToRemove1.push(Crafty.e("2D, block, block_blue").attr({x:0,y:530-140,blockColour:"blue"}));
                    blocksToRemove1.push(Crafty.e("2D, block, block_blue").attr({x:70,y:530-140,blockColour:"blue"}));
                    blocksToRemove1.push(Crafty.e("2D, block, block_blue").attr({x:0,y:530-210,blockColour:"blue"}));
                    blocksToRemove1.push(Crafty.e("2D, block, block_blue").attr({x:0,y:530-280,blockColour:"blue"}));
                    blocksToRemove1.push(Crafty.e("2D, block, block_blue").attr({x:70,y:530-280,blockColour:"blue"}));

                    blocksToRemove2.push(Crafty.e("2D, block, block_red").attr({x:210,y:530-0,blockColour:"red"}));
                    blocksToRemove2.push(Crafty.e("2D, block, block_red").attr({x:280,y:530-0,blockColour:"red"}));
                    blocksToRemove2.push(Crafty.e("2D, block, block_red").attr({x:210,y:530-70,blockColour:"red"}));
                    blocksToRemove2.push(Crafty.e("2D, block, block_red").attr({x:280,y:530-70,blockColour:"red"}));

                    blocksNotToRemove.push(Crafty.e("2D, block, block_red").attr({x:700,y:530-0,blockColour:"red"}));
                    blocksNotToRemove.push(Crafty.e("2D, block, block_red").attr({x:770,y:530-0,blockColour:"red"}));
                    blocksNotToRemove.push(Crafty.e("2D, block, block_red").attr({x:700,y:530-70,blockColour:"red"}));

                    groups.push(blocksToRemove1, blocksToRemove2, blocksNotToRemove);

                    return groups;
                });

                CombinationChecker.Check();

                var remainingBlocks = CraftyFinder.Find("block");
                expect(remainingBlocks.length).toEqual(3);

                _.forEach(blocksToRemove1, function(block)
                {
                    expect(remainingBlocks).toNotContain(block);
                });

                _.forEach(blocksToRemove2, function(block)
                {
                    expect(remainingBlocks).toNotContain(block);
                });

                _.forEach(blocksNotToRemove, function(block)
                {
                    expect(remainingBlocks).toContain(block);
                });
            });

            it("gives player 100 score for 4 connected blocks", function()
            {
                spyOn(CombinationChecker, "GetGroups").andCallFake(function()
                {
                    blocksToRemove1.push(Crafty.e("2D, block, block_blue").attr({x:0,y:530-0,blockColour:"blue"}));
                    blocksToRemove1.push(Crafty.e("2D, block, block_blue").attr({x:70,y:530-0,blockColour:"blue"}));
                    blocksToRemove1.push(Crafty.e("2D, block, block_blue").attr({x:0,y:530-70,blockColour:"blue"}));
                    blocksToRemove1.push(Crafty.e("2D, block, block_blue").attr({x:0,y:530-140,blockColour:"blue"}));

                    groups.push(blocksToRemove1);

                    return groups;
                });

                CombinationChecker.Check();

                expect(PlayerData.score).toEqual(100);
            });

            it("gives player 50+n*25 score for n (>4) connected blocks", function()
            {
                spyOn(CombinationChecker, "GetGroups").andCallFake(function()
                {
                    blocksToRemove1.push(Crafty.e("2D, block, block_blue").attr({x:0,y:530-0,blockColour:"blue"}));
                    blocksToRemove1.push(Crafty.e("2D, block, block_blue").attr({x:70,y:530-0,blockColour:"blue"}));
                    blocksToRemove1.push(Crafty.e("2D, block, block_blue").attr({x:0,y:530-70,blockColour:"blue"}));
                    blocksToRemove1.push(Crafty.e("2D, block, block_blue").attr({x:0,y:530-140,blockColour:"blue"}));
                    blocksToRemove1.push(Crafty.e("2D, block, block_blue").attr({x:70,y:530-140,blockColour:"blue"}));
                    blocksToRemove1.push(Crafty.e("2D, block, block_blue").attr({x:0,y:530-210,blockColour:"blue"}));
                    blocksToRemove1.push(Crafty.e("2D, block, block_blue").attr({x:0,y:530-280,blockColour:"blue"}));
                    blocksToRemove1.push(Crafty.e("2D, block, block_blue").attr({x:70,y:530-280,blockColour:"blue"}));

                    groups.push(blocksToRemove1);

                    return groups;
                });

                CombinationChecker.Check();

                expect(PlayerData.score).toEqual(250);
            });
        });
    });
});