/*
 * Unit Test
 * utils/logger
 */

define(["util/logger"], function()
{
    describe("util/logger", function()
    {
        var text = "A testing text to be logged in a console.";

        describe("(full logging)", function()
        {
            beforeEach(function() {
                Logger.SetThreshold(LogShow.All);
            });

            it("Logger.Log() logs a text in the debug console", function()
            {
                spyOn(console, 'log');

                Logger.Log(text);

                expect(console.log).toHaveBeenCalledWith(text);
            });

            it("Logger.Warn() logs a warning in the debug console", function()
            {
                spyOn(console, 'warn');

                Logger.Warn(text);

                expect(console.warn).toHaveBeenCalledWith(text);
            });

            it("Logger.Error() logs an error in the debug console", function()
            {
                spyOn(console, 'error');

                Logger.Error(text);

                expect(console.error).toHaveBeenCalledWith(text);
            });
        });

        describe("(errors only)", function()
        {
            beforeEach(function() {
                Logger.SetThreshold(LogShow.Errors);
            });

            it("Logger.Log() doesn't log a text in the debug console", function()
            {
                spyOn(console, 'log');

                Logger.Log(text);

                expect(console.log).wasNotCalled();
            });

            it("Logger.Warn() doesn't log a warning in the debug console", function()
            {
                spyOn(console, 'warn');

                Logger.Warn(text);

                expect(console.warn).wasNotCalled();
            });

            it("Logger.Error() logs an error in the debug console", function()
            {
                spyOn(console, 'error');

                Logger.Error(text);

                expect(console.error).toHaveBeenCalledWith(text);
            });
        });
    });
});