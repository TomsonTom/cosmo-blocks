/*
 * Crafty Finder Unit Test
 * utils/craftyfinder
 */

define(["util/craftyfinder"], function()
{
    describe("util/craftyfinder", function()
    {
        var entitiesA = []; // Entities with component "A"
        var entitiesB = []; // Entities with component "B"

        var entitiesA_ids = []; // IDs of entities with component "A"
        var entitiesB_ids = []; // IDs of entities with component "B"

        beforeEach(function()
        {
            for (var i = 0; i <= 5; i++)
            {
                entitiesA[i] = Crafty.e("A");
                entitiesB[i] = Crafty.e("B");

                entitiesA_ids[i] = entitiesA[i][0];
                entitiesB_ids[i] = entitiesB[i][0];
            }
        });

        afterEach(function()
        {
            for (var i = 0; i <= 5; i++)
            {
                entitiesA[i].destroy();
                entitiesB[i].destroy();
            }
        });

        it("CraftyFinder.FindIds('component') returns array of IDs of entities with 'component'", function()
        {
            var arrayA = CraftyFinder.FindIds('A');
            var arrayB = CraftyFinder.FindIds('B');

            // Note: we should sort the arrays from lowest to highest ID before comparing
            expect(_.sortBy(arrayA, function(num){return num;})).toEqual(_.sortBy(entitiesA_ids, function(num){return num;}));
            expect(_.sortBy(arrayB, function(num){return num;})).toEqual(_.sortBy(entitiesB_ids, function(num){return num;}));
        });

        it("CraftyFinder.Find('component') returns array of entities with 'component'", function()
        {
            var arrayA = CraftyFinder.Find('A');
            var arrayB = CraftyFinder.Find('B');

            expect(arrayA).toEqual(entitiesA);
            expect(arrayB).toEqual(entitiesB);
        });
    });
});