/*
 * ================================================================
 *
 *   _____                            ____  _            _
 *  / ____|                          |  _ \| |          | |
 * | |     ___  ___ _ __ ___   ___   | |_) | | ___   ___| | _____
 * | |    / _ \/ __| '_ ` _ \ / _ \  |  _ <| |/ _ \ / __| |/ / __|
 * | |___| (_) \__ \ | | | | | (_) | | |_) | | (_) | (__|   <\__ \
 *  \_____\___/|___/_| |_| |_|\___/  |____/|_|\___/ \___|_|\_\___/
 *
 *  MIT LICENSE                          source code by Tomáš Iser
 * ================================================================
 *
 *  Unit Tests - Init
 *  -----------------
 */

require(
    {
        paths:
        {   //name: 'path'
            ui:     '../src/ui',
            net:    '../src/network',
            ent:    '../src/entities',
            com:    '../src/components',
            scene:  '../src/scenes',
            util:   '../src/utils',

            spec_ui:     'ui',
            spec_net:    'network',
            spec_ent:    'entities',
            spec_com:    'components',
            spec_scene:  'scenes',
            spec_util:   'utils'
        }
    },

    [   // List of files to load for unit testing
        "../lib/domReady",
        "spec_util/logger",
        "spec_util/spritemanager",
        "spec_com/cloud",
        "spec_util/craftyfinder",
        "spec_util/combinationchecker"
    ],

    function(domReady)
    {
        Crafty.init(1,1);

        var jasmineEnv = jasmine.getEnv();
        jasmineEnv.updateInterval = 1000;

        var htmlReporter = new jasmine.HtmlReporter();

        jasmineEnv.addReporter(htmlReporter);

        jasmineEnv.specFilter = function(spec) {
            return htmlReporter.specFilter(spec);
        };

        var currentWindowOnload = window.onload;

        domReady(function() {
            execJasmine();
        });

        function execJasmine() {
            console.log("Executing Jasmine");
            jasmineEnv.execute();
        }
    }
);