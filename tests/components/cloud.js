/*
 * Cloud Component Test
 * components/cloud
 */

define(["com/cloud"], function()
{
    describe("com/cloud", function()
    {
        it("entity has a random cloud sprite [loop 100]", function()
        {
            var cloud;

            for (var i = 100; i > 0; i--) {
                cloud = Crafty.e("2D, Cloud");

                expect(cloud.has("cloud_1") || cloud.has("cloud_2") || cloud.has("cloud_3")).toBeTruthy();

                cloud.destroy();
            }
        });

        it("entity is located in the sky [loop 100]", function()
        {
            var cloud;

            for (var i = 100; i > 0; i--) {
                cloud = Crafty.e("2D, Cloud");

                expect(cloud.y >= 0).toBeTruthy();
                expect(cloud.y <= 300).toBeTruthy();

                cloud.destroy();
            }
        });

        it("entity is visible at the beginning [loop 100]", function()
        {
            var cloud;

            for (var i = 100; i > 0; i--) {
                cloud = Crafty.e("2D, Cloud");

                expect(cloud.x >= 0).toBeTruthy();
                expect(cloud.x <= (840-200)).toBeTruthy();

                cloud.destroy();
            }
        });

        it("entity is moving right", function()
        {
            var cloud;

            cloud = Crafty.e("2D, Cloud").attr({x:0});

            var oldX, newX, flag;
            oldX = cloud.x;

            runs(function() {
                setTimeout(function() {flag = true;}, 100);
            });

            waitsFor(function(){return flag;}, "100 ms timer", 200);

            runs(function() {
                var newX = cloud.x;
                expect(newX).toBeGreaterThan(oldX);
                cloud.destroy();
            });
        });

        it("entity should reappear on the left side after it disappears", function()
        {
            var cloud, flag;

            cloud = Crafty.e("2D, Cloud").attr({x:820,speed:10});

            runs(function() {
                setTimeout(function() {flag = true;}, 200);
            });

            waitsFor(function(){return flag;}, "200 ms timer", 300);

            runs(function() {
                expect(cloud.x).toBeLessThan(800);
                cloud.destroy();
            });
        });

        it("entity with higher Y should have higher Z (lower = on top)", function()
        {
            var clouds = [];

            for (var i = 5; i >= 0; i--)
            {
                clouds[i] = Crafty.e("2D, Cloud");
            }

            _.forEach(clouds, function(cloud)
            {
                _.forEach(clouds, function(cloud2)
                {
                         if (cloud.y > cloud2.y)  { expect(cloud.z).toBeGreaterThan(cloud2.z); }
                    else if (cloud.y < cloud2.y)  { expect(cloud.z).toBeLessThan(cloud2.z); }
                    else if (cloud.y == cloud2.y) {  }
                });
            });

            for (i = 5; i >= 0; i--)
            {
                clouds[i].destroy();
            }
        });
    });
});