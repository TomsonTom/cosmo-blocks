({
    baseUrl: "src",
    paths: {
        ui:     'ui',
        net:    'network',
        ent:    'entities',
        com:    'components',
        scene:  'scenes',
        util:   'utils',
        img:    '../img'
    },
    name: "main",
    out: "cosmoblocks.js"
})