/*
 * Application.js
 */

define(["scene/loading"], function()
{
    /**
     * Represents a Bubble Snake application.
     * @static
     */
    Application = function()
    {
    };

    /**
     * Initializes the application.
     */
    Application.Init = function()
    {
        Crafty.init(840, 600);
        Crafty.canvas.init();

        Crafty.scene("loading");

        Logger.Log("Initialized.");
    };

    /**
     * true on pause, false on running
     */
    Application.isPaused = false;

    /**
     * Represents player data that should be shared all over the application.
     * @static
     */
    PlayerData = {};
    PlayerData.score = 0;
});