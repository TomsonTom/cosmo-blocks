/*
 * ================================================================
 *
 *   _____                            ____  _            _
 *  / ____|                          |  _ \| |          | |
 * | |     ___  ___ _ __ ___   ___   | |_) | | ___   ___| | _____
 * | |    / _ \/ __| '_ ` _ \ / _ \  |  _ <| |/ _ \ / __| |/ / __|
 * | |___| (_) \__ \ | | | | | (_) | | |_) | | (_) | (__|   <\__ \
 *  \_____\___/|___/_| |_| |_|\___/  |____/|_|\___/ \___|_|\_\___/
 *
 *  MIT LICENSE                          source code by Tomáš Iser
 * ================================================================
 *
 *  The Game - Init
 *  ---------------
 */

require(
    {
        paths:
        {
            ui:     'ui',
            net:    'network',
            ent:    'entities',
            com:    'components',
            scene:  'scenes',
            util:   'utils',
            img:    '../img'
        }
    },

    ['application', 'util/logger', 'util/craftyfinder'],

    function()
    {
        Logger.SetThreshold(LogShow.All);
        Logger.Log("Initializing application.");
        Application.Init();
    }
);