/*
 * Game Over Scene
 * scenes/gameover
 */

define(["util/spritemanager", "img/imglist", "scene/gametest", "scene/menu"], function(SpriteManager)
{
    Crafty.scene("gameover", function ()
    {
        Crafty.background("#222");

        // Kill static entities if they're defined
        _.forEach(CraftyFinder.Find("BlockSpawner"), function(e) {e.destroy();});
        _.forEach(CraftyFinder.Find("KeyboardListener"), function(e) {e.destroy();});
        _.forEach(CraftyFinder.Find("CombiCheckLauncher"), function(e) {e.destroy();});

        // Keyboard listener
        // P = pause, C = toggle clouds, R = reset to menu
        var keyListener = Crafty.e("KeyboardListener");

        Crafty.e("2D, DOM, Text")
              .attr({ w: 300, h: 20, x: 270, y: 40 })
              .text("Game Over")
              .css({'color': 'white', 'font-size': '48px', 'font-family': 'Tahoma', 'text-align': 'center'});
        Crafty.e("2D, DOM, Text")
              .attr({ w: 250, h: 20, x: 295, y: 250 })
              .text("Score: " + PlayerData.score)
              .css({'color': 'white', 'font-size': '24px', 'font-family': 'Tahoma', 'text-align': 'center'});
        Crafty.e("2D, DOM, Text")
              .attr({ w: 250, h: 20, x: 295, y: 350 })
              .text("Press R to return to menu.")
              .css({'color': 'white', 'font-size': '12px', 'font-family': 'Tahoma', 'text-align': 'center'});
    });
});