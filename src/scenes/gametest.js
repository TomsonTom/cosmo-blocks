/*
 * Game Test Scene
 * scenes/gametest
 */

define(["util/environment", "com/player", "com/blockspawner", "com/keylistener", "com/combichecklauncher"], function(Environment)
{
    Crafty.scene("gametest", function ()
    {
        Crafty.background("#cdf3f7");

        // Set up environment
        var env = new Environment();
        env.GenerateFloor();
        env.GenerateVegetation();
        env.GenerateFences();
        env.GenerateClouds(20);

        // Set up player
        var player = Crafty.e("2D, DOM, Player, solid");

        // Set up block spawning
        var blockSpawner = Crafty.e("BlockSpawner");

        // Keyboard listener
        // P = pause, C = toggle clouds
        var keyListener = Crafty.e("KeyboardListener");

        // Set up combination checker
        var combiCheckLauncher = Crafty.e("CombiCheckLauncher");

        // Set up GUI showing score
        var guiScore = Crafty.e("2D, DOM, Text")
                             .bind("CombinationsChecked", function(){this.text("Score: " + PlayerData.score);})
                             .attr({ w: 150, h: 20, x: 15, y: 15 })
                             .text("Score: 0")
                             .css({'font-size': '18px', 'font-family': 'Tahoma'});
    });
});