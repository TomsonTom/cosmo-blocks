/*
 * Loading Scene
 * scenes/loading
 */

define(["util/spritemanager", "img/imglist", "scene/gametest", "scene/menu"], function(SpriteManager)
{
    var loadingText = "";
    var sprman;
    var withoutError = true;
    var notFoundFiles = [];
    var errorLog;

    Crafty.scene("loading", function ()
    {
        Crafty.background("#222");
        Crafty.e("2D, DOM, Text")
              .attr({ w: 300, h: 20, x: 270, y: 40 })
              .text("Cosmo Blocks")
              .css({'color': 'white', 'font-size': '48px', 'font-family': 'Tahoma', 'text-align': 'center'});
        Crafty.e("2D, DOM, Text")
              .attr({ w: 250, h: 20, x: 295, y: 460 })
              .text("Initializing the game, please wait...")
              .css({'color': 'white', 'font-size': '12px', 'font-family': 'Tahoma', 'text-align': 'center'});

        Crafty.e("2D, DOM, Text")
              .attr({ w: 400, h: 20, x: 220, y: 557 })
              .text("Source code by Tomáš Iser, shared under MIT license, available on BitBucket.<br>Graphics by Kenney.nl, they may be used in personal and commercial projects.<br>This game is using Crafty.js, Underscore.js and RequireJS frameworks.")
              .css({'color': '#AAA', 'font-size': '10px', 'font-family': 'Tahoma', 'text-align': 'center'});

        loadingText = Crafty.e("2D, DOM, Text")
                            .attr({ w: 250, h: 20, x: 295, y: 490 })
                            .text("Loading game files...<br>(Loaded 0/7)")
                            .css({'color': 'white', 'font-size': '12px', 'font-family': 'Tahoma', 'text-align': 'center'});

        LoadFiles();
    });

    function LoadFiles()
    {
        sprman = new SpriteManager(sprites);
        sprman.Load(onLoad, onProgress, onError);
    }

    function onLoad()
    {
        sprman.Create();
        Logger.Log(Crafty.assets);
        Crafty.e("2D, Canvas, male2")
            .attr({ w: 300, h: 100, x: 20, y: 300, z: 1 });

            Crafty.e("2D, DOM, male2")
            .attr({ w: 300, h: 100, x: 20, y: 420, z: 1 });

        // Let's wait a second and load the game :)
        if (withoutError === true)
            Crafty.e("Delay").delay(function(){Crafty.scene("menu");}, 1000);
    }

    function onProgress(info)
    {
        if (info.loaded != info.total)
            loadingText.text("Loading game files...<br>(Loaded " + info.loaded + "/" + info.total + ")");
        else
            loadingText.text("All game files were loaded.<br>Game will start in a second.");
    }

    function onError(asset)
    {
        withoutError = false;
        notFoundFiles.push(asset.src);

        if (errorLog !== undefined)
            errorLog.destroy();

        var errorText = "";
        _.forEach(notFoundFiles, function(file)
        {
            errorText += file + ",";
        });

        errorLog = Crafty.e("2D, DOM, Text")
                         .attr({ w: 400, h: 20, x: 220, y: 100 })
                         .text("<textarea rows=5 cols=50>ERROR:\nCouldn't load file(s) " + errorText + "</textarea><input type='button' value='Skip errors' onclick='Crafty.scene(\"menu\")'>")
                         .css({'font-size': '12px', 'font-family': 'Tahoma', 'color': 'red'});
    }
});