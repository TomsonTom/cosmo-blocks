/*
 * Menu Scene
 * scenes/menu
 */

define(["util/environment", "com/blockspawner", "com/keylistener", "com/combichecklauncher", "util/craftyfinder", "scene/freeplay", "scene/challenge"], function(Environment)
{
    Crafty.scene("menu", function ()
    {
        Crafty.background("#cdf3f7");

        // Kill static entities if they're defined
        _.forEach(CraftyFinder.Find("BlockSpawner"), function(e) {e.destroy();});
        _.forEach(CraftyFinder.Find("KeyboardListener"), function(e) {e.destroy();});
        _.forEach(CraftyFinder.Find("CombiCheckLauncher"), function(e) {e.destroy();});

        // Reset score
        PlayerData.score = 0;

        // Set up environment
        var env = new Environment();
        env.GenerateFloor();
        env.GenerateVegetation();
        env.GenerateFences();
        env.GenerateClouds(20);

        // Set up block spawning
        var blockSpawner = Crafty.e("BlockSpawner");

        // Keyboard listener
        // P = pause, C = toggle clouds, R = reset to menu
        var keyListener = Crafty.e("KeyboardListener");

        // Set up combination checker
        var combiCheckLauncher = Crafty.e("CombiCheckLauncher");

        // Set up logo
        Crafty.e("2D, DOM, Text")
              .attr({ w: 300, h: 20, x: 270, y: 40 })
              .text("Cosmo Blocks")
              .css({'color': 'white', 'font-size': '48px', 'font-family': 'Tahoma', 'text-align': 'center'});

        // Set up license notice
        Crafty.e("2D, DOM, Text")
              .attr({ w: 400, h: 20, x: 220, y: 557 })
              .text("Source code by Tomáš Iser, shared under MIT license, available on BitBucket.<br>Graphics by Kenney.nl, they may be used in personal and commercial projects.<br>This game is using Crafty.js, Underscore.js and RequireJS frameworks.")
              .css({'color': '#333', 'font-size': '10px', 'font-family': 'Tahoma', 'text-align': 'center'});

        // Set up help text
        Crafty.e("2D, DOM, Text, Keyboard")
              .attr({ w: 400, h: 20, x: 220, y: 250 })
              .text("Press A or Left Arrow to play free mode.<br>Press D or Right Arrow to play challenge.")
              .css({'color': 'black', 'font-size': '18px', 'font-family': 'Tahoma', 'text-align': 'center'})
              .bind("KeyDown", function()
                                {
                                    if (this.isDown('A') || this.isDown('LEFT_ARROW'))  Crafty.scene("freeplay");
                                    if (this.isDown('D') || this.isDown('RIGHT_ARROW')) Crafty.scene("challenge");
                                });
        Crafty.e("2D, DOM, Text")
              .attr({ w: 400, h: 20, x: 220, y: 310 })
              .text("In challenge mode, the game ends when you stack up more blocks than fits on the screen, and also the delay between spawning will lower as you play, so the game gets harder.")
              .css({'color': 'black', 'font-size': '14px', 'font-family': 'Tahoma', 'text-align': 'center'});
        Crafty.e("2D, DOM, Text")
              .attr({ w: 500, h: 20, x: 170, y: 120 })
              .text("The goal of the game is to get as high score as possible.<br>You get 100 points if there are 4 blocks connected,<br>and even more score if you connect more than 4 blocks!<br><br>Move player using A/D key or Left/Right arrow.<br>Jump using W key or Up arrow.<br>Push blocks by pressing SPACE key.")
              .css({'color': 'black', 'font-size': '14px', 'font-family': 'Tahoma', 'text-align': 'center'});
    });
});