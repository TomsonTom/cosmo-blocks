/*
 * Freeplay Scene
 * scenes/freeplay
 */

define(["util/environment", "com/player", "com/blockspawner", "com/keylistener", "com/combichecklauncher"], function(Environment)
{
    Crafty.scene("freeplay", function ()
    {
        Crafty.background("#cdf3f7");

        // Kill static entities if they're defined
        _.forEach(CraftyFinder.Find("BlockSpawner"), function(e) {e.destroy();});
        _.forEach(CraftyFinder.Find("KeyboardListener"), function(e) {e.destroy();});
        _.forEach(CraftyFinder.Find("CombiCheckLauncher"), function(e) {e.destroy();});

        // Reset score
        PlayerData.score = 0;

        // Set up environment
        var env = new Environment();
        env.GenerateFloor();
        env.GenerateVegetation();
        env.GenerateFences();
        env.GenerateClouds(20);

        // Set up player
        var player = Crafty.e("2D, DOM, Player, solid");

        // Set up block spawning
        var blockSpawner = Crafty.e("BlockSpawner");

        // Keyboard listener
        // P = pause, C = toggle clouds, R = reset to menu
        var keyListener = Crafty.e("KeyboardListener");

        // Set up combination checker
        var combiCheckLauncher = Crafty.e("CombiCheckLauncher");

        // Set up GUI
        var guiScore = Crafty.e("2D, DOM, Text")
                             .bind("CombinationsChecked", function(){this.text("Score: " + PlayerData.score);})
                             .attr({ w: 150, h: 20, x: 15, y: 15 })
                             .text("Score: 0")
                             .css({'font-size': '18px', 'font-family': 'Tahoma'});
        Crafty.e("2D, DOM, Text")
              .attr({ w: 150, h: 20, x: 15, y: 40 })
              .text("Freeplay Mode")
              .css({'font-size': '12px', 'font-family': 'Tahoma'});
    });
});