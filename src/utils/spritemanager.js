/*
 * Sprite Manager
 * utils/spritemanager
 */

define([], function()
{
    function SpriteManager(source)
    {
		this.source = source;
    }

	SpriteManager.prototype.GetPaths = function()
	{
		var paths = [];
		_.forEach(this.source, function(element, key)
		{
			paths.push(element["path"]);
		});
		return paths;
	};

	SpriteManager.prototype.Load = function(onLoad, onProgress, onError)
	{
		Crafty.load(this.GetPaths(), onLoad, onProgress, onError);
	};

	SpriteManager.prototype.Create = function()
	{
		_.forEach(this.source, function(element, key)
		{
			if (element["path"] !== undefined)
			{
				// If the sprite hasn't already been loaded, we'll load it
				if (Crafty.asset(element.path) === undefined)
					Crafty.load(element.path);

				// If the elements are defined (= we don't need to manually determine anything)
				if (element["elements"] !== undefined)
				{
					Crafty.sprite(element["tile_width"], element["tile_height"], element["path"], element["elements"],
						element["padding_x"], element["padding_y"]);
				}
				// If the elements aren't defined, it means the sprite consist of one element only
				// and we have to determine the size of the image manually and create the elements array
				else
				{
					var img = Crafty.asset(element.path);
					if (img !== undefined)
					{
						img.src = element.path;
						var elements = {};
						elements[key] = [0, 0, img.width, img.height];
						Crafty.sprite(element.path, elements);
					}
				}
			}
		});
	};

    return SpriteManager;
});