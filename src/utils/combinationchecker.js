/*
 * Blocks Combination Checker
 * utils/combinationchecker
 *
 * This object checks blocks.
 *
 * If 4 blocks of the same colour are touching, they're removed and player gains 100 score.
 * If 'n' (n > 4) blocks are touching, they're removed and player gains 50+n*25 score.
 */

define(["scene/gameover"], function()
{
    CombinationChecker = {};

    CombinationChecker.GetGroups = function()
    {
        var allBlocks = CraftyFinder.Find("block"); // All blocks in game
        var separatedBlocks = {yellow:[], red:[], green:[], blue:[]};

        // Ignore blocks that are falling and that aren't in a grid (i.e. are being pushed)
        // and divide them into categories by colour
        _.forEach(allBlocks, function(block)
        {
            // 530 is the 'y' coord of floor
            if ((block._falling === undefined || block._falling !== true) && (block.x % 70) === 0 && ((530 - block.y) % 70) === 0)
            {
                separatedBlocks[block.blockColour].push(block);
            }
        });

        var groups = []; // Groups of blocks with the same colour that touches
        var checked = []; // Already checked blocks
        _.forEach(separatedBlocks, function(blocks, colour)
        {
            var whereToSearch = blocks;

            _.forEach(blocks, function(block)
            {
                if (!_.contains(checked, block))
                {
                    var group = CombinationChecker.FindGroup(block, whereToSearch);
                    groups.push(group);

                    whereToSearch = _.difference(whereToSearch, group);
                    checked = checked.concat(group);
                }
            });
        });

        return groups;
    };

    CombinationChecker.FindNear = function(block, array)
    {
        var near = [];

        _.forEach(array, function(blk)
        {
            //                        Left                     Right
            if ((blk.y == block.y && (blk.x == (block.x-70) || blk.x == (block.x+70))) ||
            //                        Above                    Below
                (blk.x == block.x && (blk.y == (block.y-70) || blk.y == (block.y+70))))
                near.push(blk);
        });

        return near;
    };

    CombinationChecker.FindGroup = function(block, array)
    {
        var group = [block];
        var whereToSearch = _.without(array, block);
        var near = CombinationChecker.FindNear(block, whereToSearch);
        var near2 = [];

        while (near.length > 0)
        {
            group = group.concat(near);

            whereToSearch = _.difference(whereToSearch, near);

            _.forEach(near, function(nearBlock)
            {
                near2 = near2.concat(CombinationChecker.FindNear(nearBlock, whereToSearch));
            });

            near = near2.slice(0);
            near2 = [];
        }

        return group;
    };

    CombinationChecker.Check = function()
    {
        var allGroups = CombinationChecker.GetGroups();

        _.forEach(allGroups, function(group)
        {
            if (group.length >= 4)
            {
                _.forEach(group, function(block)
                {
                    block.destroy();
                });

                if (group.length == 4)
                    PlayerData.score += 100;
                else
                    PlayerData.score += 50 + 25*group.length;
            }
        });

        Crafty.trigger("CombinationsChecked");
    };

    CombinationChecker.CheckGameOver = function()
    {
        var blocks = CraftyFinder.Find("block");
        _.forEach(blocks, function(block)
        {
            if (block._falling === false && block.y < 0)
                Crafty.scene("gameover");
        });
    };
});