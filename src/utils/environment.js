/*
 * Environment
 * utils/environment
 *
 * NO UNIT TEST
 * must be checked visually
 */

define(["com/cloud"], function()
{
    var vegetationTypes = [
        'grass',
        'rock',
        'mushroom',
        'hill_short',
        'hill_long'
    ];

    function Environment(blocksPerLine)
    {
        if (blocksPerLine === undefined)
            this.blocksPerLine = 12;
        else
            this.blocksPerLine = blocksPerLine;

        // Contains:
        //  -1 if no vegetation is located above the block, or
        //  vegetation entity if it's located above the block
        this.vegetation = [];
        for (var i = this.blocksPerLine - 1; i >= 0; i--)
        {
            if (i > 0 && i < (this.blocksPerLine - 1))  // Leftmost and rightmost blocks will have a fence
                this.vegetation[i] = -1;
        }
    }

    Environment.prototype.GenerateFloor = function()
    {
		for (var i = this.blocksPerLine - 1; i >= 0; i--)
		{
			Crafty.e("2D, Canvas, solid, ground_grass")
				.attr({ x: i*70, y: 530, z: 1 });
        }
    };

    Environment.prototype.GenerateVegetation = function()
    {
        var lastUsedVegType = -1;
        var shortHills = 0;
        var longHills = 0;

        _.forEach(this.vegetation, function(veg, index)
        {
            // Remove existing vegetation from block
            if (veg != -1 && veg !== undefined)
            {
                veg.destroy();
                veg = -1;
            }

            // Add new random vegetation
            // explanation: a number between 0 and (vegetation types count + 2) will be generated;
            //              if a generated number matches a vegetation type, it'll be used,
            //              however if the number won't match an existing veg type, no vegetation
            //              will be present at the block;
            //              the chance of no vegetation is raised by adding '+2' to the length of array
            var vegType = Math.round(Math.random()*(vegetationTypes.length+2));

            // If there'll be a vegetation
            if (vegType < vegetationTypes.length)
            {
                // We have to be sure that there aren't two same kinds of vegetation next to each other
                // and also there should be max 1 short and 1 long hill, otherwise it'd look weird
                while ( vegType == lastUsedVegType ||
                       (vegType == _.indexOf(vegetationTypes, 'hill_long')  && longHills > 0) ||
                       (vegType == _.indexOf(vegetationTypes, 'hill_short') && shortHills > 0) )
                {
                    vegType = Math.round(Math.random()*(vegetationTypes.length-1)); // Generate new kind of veg
                }

                // Offset within a block
                var offset = 0;
                     if (vegType == _.indexOf(vegetationTypes, 'rock'))     { offset = 0; }
                else if (vegType == _.indexOf(vegetationTypes, 'grass'))    { offset = Math.random()*35; }
                else if (vegType == _.indexOf(vegetationTypes, 'mushroom')) { offset = Math.random()*25; }
                else if (vegType == _.indexOf(vegetationTypes, 'hill_short') || vegType == _.indexOf(vegetationTypes, 'hill_long'))
                                                                            { offset = 5 + Math.random()*10; }

                // Increment number of short / long hills if necessary
                     if (vegType == _.indexOf(vegetationTypes, 'hill_short')) { shortHills++; }
                else if (vegType == _.indexOf(vegetationTypes, 'hill_long'))  { longHills++; }

                Crafty.e("2D, Canvas, vegetation, " + vegetationTypes[vegType])
                      .attr({ x: index*70 + offset, y: 384, z: 0 });
            }

            lastUsedVegType = vegType;
        });
    };

    Environment.prototype.GenerateFences = function()
    {
        // Left fence
        var fenceType = Math.round(Math.random());
        fenceType = (fenceType == 1) ? "fence_new" : "fence_broken";
        Crafty.e("2D, Canvas, fence, " + fenceType)
              .attr({ x: 0, y: 469, z: 0 });

        // Right fence
        fenceType = Math.round(Math.random());
        fenceType = (fenceType == 1) ? "fence_new" : "fence_broken";
        Crafty.e("2D, Canvas, fence, " + fenceType)
              .attr({ x: 70*(this.blocksPerLine-1), y: 469, z: 0 });
    };

    Environment.prototype.GenerateClouds = function(howManyClouds)
    {
        for (var i = howManyClouds - 1; i >= 0; i--)
        {
            Crafty.e("2D, Canvas, Cloud");
        }
    };

    return Environment;
});