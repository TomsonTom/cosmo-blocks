/*
 * sprites.js
 */

require(
    {
        paths:
        {
            ui:     'ui',
            net:    'network',
            ent:    'entities',
            com:    'components',
            scene:  'scenes',
            util:   'utils'
        }
    },
    
    ['application'],
    
    function()
    {
        Application.Init();
    }
);