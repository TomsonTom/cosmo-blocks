/*
 * Logger Object
 * utils/logger
 */

define([], function()
{
    LogShow = {
        Errors: 1,
        All: 2
    };

    /* Logger object */
    Logger = {};

    // The logger will log everything until someone changes the configuration
    Logger.Threshold = LogShow.All;

    /*
    * Sets the threshold of the logger.
    * @param threshold What will be logged (use LogShow object).
    */
    Logger.SetThreshold = function(threshold)
    {
        if (Number(threshold) == LogShow.Errors)
            Logger.Threshold = LogShow.Errors;
        else
            Logger.Threshold = LogShow.All;
    };

    /*
    * Logs a piece of information.
    * @param text What to log.
    */
    Logger.Log = function(text)
    {
        if (text !== null && Logger.Threshold == LogShow.All)
            console.log(text);
    };

    /*
    * Logs a warning in the console.
    * @param text Warning description.
    */
    Logger.Warn = function(text)
    {
        if (text !== null && Logger.Threshold == LogShow.All)
            console.warn(text);
    };

    /*
    * Logs an error in the console.
    * @param text Error description.
    */
    Logger.Error = function(text)
    {
        if (text !== null)
            console.error(text);
    };
});