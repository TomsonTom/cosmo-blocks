/*
 * Crafty Finder
 * utils/craftyfinder
 */

define([], function()
{
    CraftyFinder = {};

    CraftyFinder.FindIds = function(component)
    {
        var array = Crafty(component);
        var ids = [];

        _.forEach(array, function(value, key)
        {
            // If the key is numeric, its value is an ID of an entity
            if (isNumber(key))
                ids.push(value);
        });

        return ids;
    };

    CraftyFinder.Find = function(component)
    {
        var ids = CraftyFinder.FindIds(component);
        var entities = [];

        _.forEach(ids, function(id)
        {
            entities.push(Crafty(id));
        });

        return entities;
    };

    function isNumber(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }
});