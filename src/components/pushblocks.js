/*
 * Push Blocks Component
 * com/pushblocks
 *
 * NO UNIT TEST
 *
 * This component allows player to push blocks.
 */

define(["util/craftyfinder"], function()
{
    Crafty.c("PushBlocks",
    {
        init: function()
        {
            this.requires("Player, Facing, Keyboard");

            this.bind("KeyDown", this._onKeyPressed);
            this.bind("EnterFrame", this._highlight);
        },

        _onKeyPressed: function()
        {
            if (this.isDown('SPACE'))
            {
                var player = this;
                var pos = this.pos();
                pos._h -= 50;
                pos._y += 50;
                pos._x += this._facing * 10;

                var objs = Crafty.map.search(pos); // Objects in front of player

                _.forEach(objs, function(obj)
                {
                    if (obj.has("Pushable"))
                        obj.tryPush(player._facing);
                });
            }
        },

        _highlight: function()
        {
            var player = this;
            var pos = this.pos();
            pos._h -= 50;
            pos._y += 50;
            pos._x += this._facing * 10;

            var objs = Crafty.map.search(pos); // Objects in front of player
            var notHighlighted = _.difference(CraftyFinder.Find("Pushable"), objs);

            _.forEach(objs, function(obj)
            {
                if (obj.has("Pushable"))
                {
                    obj.alpha = 0.7;
                }
            });

            _.forEach(notHighlighted, function(obj)
            {
                obj.alpha = 1.0;
            });
        }
    });
});