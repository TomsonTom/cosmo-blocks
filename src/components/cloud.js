/*
 * Cloud Component
 * com/cloud
 */

define([], function()
{
    Crafty.c("Cloud",
    {
        init: function()
        {
            this.requires("2D");

            var cloudType = Math.round(Math.random()*2)+1;
            this.addComponent("cloud_" + cloudType);

            this.x = Math.round(Math.random()*630); // x = (0 - 630)
            this.y = Math.round(Math.random()*300); // y = (0 - 300)
            this.speed = 0.1 + Math.random()*1;

            this._recalculateCloudsZ();

            this.bind("EnterFrame", this._enterFrame);
        },

        // Recalculates and resets all clouds' Z (the lowest clouds are on top the other clouds)
        _recalculateCloudsZ: function()
        {
            var clouds = CraftyFinder.Find("Cloud");
            clouds = _.sortBy(clouds, function(cloud){return cloud.y;}); // Sort clouds by their Y

            _.forEach(clouds, function(cloud, index)
            {
                // Lowest Z = 1, highest Z = 1 + cloudsCount
                cloud.z = 1 + index;
            });
        },

        _enterFrame: function()
        {
            this.x += this.speed;

            // Refresh clouds that can't be seen
            if (this.x > 830)
            {
                this._refresh(-200);
            }
        },

        _refresh: function(x)
        {
            if (x === undefined) {x=Math.round(Math.random()*630);}

            this.x = x;
            this.y = Math.round(Math.random()*300); // y = (0 - 300)
            this.speed = 0.1 + Math.random()*1;
            this._recalculateCloudsZ();
        }
    });
});