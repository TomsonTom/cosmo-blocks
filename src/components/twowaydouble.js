/*
 * Twoway Double Jump Component
 * com/twowaydouble
 *
 * NO UNIT TEST
 *
 * Enhanced Twoway component that allows double jumping
 */

define(["com/facing", "com/gravityplus"], function()
{
    /**@
    * #Twoway
    * @category Input
    * Move an entity left or right using the arrow keys or `D` and `A` and jump using up arrow or `W`.
    *
    * When direction changes a NewDirection event is triggered with an object detailing the new direction: {x: x_movement, y: y_movement}. This is consistent with Fourway and Multiway components.
    * When entity has moved on x-axis a Moved event is triggered with an object specifying the old position {x: old_x, y: old_y}
    */
    Crafty.c("TwowayDouble", {
        _speed: 3,
        _up: false,
        _upPressed: false,

        init: function () {
            this.requires("Fourway, Keyboard");
        },

        /**@
        * #.twoway
        * @comp Twoway
        * @sign public this .twoway(Number speed[, Number jumpSpeed])
        * @param speed - Amount of pixels to move left or right
        * @param jumpSpeed - How high the entity should jump
        *
        * Constructor to initialize the speed and power of jump. Component will
        * listen for key events and move the entity appropriately. This includes
        * ~~~
        * `Up Arrow`, `Right Arrow`, `Left Arrow` as well as W, A, D. Used with the
        * `gravity` component to simulate jumping.
        * ~~~
        *
        * The key presses will move the entity in that direction by the speed passed in
        * the argument. Pressing the `Up Arrow` or `W` will cause the entity to jump.
        *
        * @see Gravity, Fourway
        */
        twoway: function (speed, jump) {

            this.multiway(speed, {
                RIGHT_ARROW: 0,
                LEFT_ARROW: 180,
                D: 0,
                A: 180,
                Q: 180
            });

            if (speed) this._speed = speed;
            jump = jump || this._speed * 2;

            this.bind("EnterFrame", function () {
                if (this._up) {
                    this.y -= jump;
                    this._falling = true;
                }
                if (this._up2) {
                    this.y -= jump;
                }
            }).bind("KeyDown", function () {
                if (this.isDown("UP_ARROW") || this.isDown("W") || this.isDown("Z"))
                {
                    if (this._up === true && this._upPressed === false)
                        this._up2 = true;

                    this._up = true;
                    this._upPressed = true;
                }
            }).bind("KeyUp", function () {
                if (!this.isDown("UP_ARROW") && !this.isDown("W") && !this.isDown("Z"))
                    this._upPressed = false;
            });

            return this;
        }
    });
});