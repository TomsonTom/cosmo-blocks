/*
 * Facing Component
 * com/facing
 *
 * NO UNIT TEST
 */

define([], function()
{
    FacingDirection = { LEFT: -1, RIGHT: 1 };

    Crafty.c("Facing",
    {
        init: function()
        {
            this.requires("2D");
            this._facingDefault = FacingDirection.LEFT;
            this._facing = this._facingDefault;
        },

        facing: function(defaultDir)
        {
            this._facingDefault = defaultDir;
            this._facing = this._facingDefault;
        },

        face: function(dir)
        {
            if (this._facing != dir)
            {
                this._facing = dir;

                if (this._facing != this._facingDefault)
                    this.flip("X");
                else
                    this.unflip("X");
            }
        }
    });
});