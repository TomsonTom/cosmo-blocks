/*
 * Keyboard Listener Component
 * com/keylistener
 *
 * NO UNIT TEST
 *
 * This component allows player to pause game and toggle clouds.
 */

define(["util/environment", "util/craftyfinder"], function(Environment)
{
    Crafty.c("KeyboardListener",
    {
        init: function()
        {
            this.pauseText = null;
            this.clouds = 20;
            this.requires("Keyboard");
            this.bind("KeyDown", this._onKeyPressed);
        },

        _onKeyPressed: function()
        {
            if (this.isDown('P'))
            {
                Application.isPaused = Application.isPaused ? false : true;
                if (Application.isPaused)
                {
                    this.pauseText = Crafty.e("2D, DOM, Text")
                                           .bind("Change", function(){Crafty.pause();})
                                           .attr({ w: 150, h: 20, x: 50, y: 65 })
                                           .text("Game Paused")
                                           .css({'font-size': '24px', 'font-family': 'Tahoma'});
                }
                else
                {
                    Crafty.pause();
                    this.pauseText.destroy();
                }
            }
            if (this.isDown('C'))
            {
                var env = new Environment();

                if (this.clouds == 20)
                {
                    this.clouds = 0;
                    var clouds = CraftyFinder.Find("Cloud");
                    _.forEach(clouds, function(cloud)
                    {
                        cloud.destroy();
                    });
                }
                else if (this.clouds === 0)
                {
                    this.clouds = 3;
                    env.GenerateClouds(3);
                }
                else if (this.clouds == 3)
                {
                    this.clouds = 20;
                    env.GenerateClouds(20);
                }
            }
            if (this.isDown('R'))
                Crafty.scene("menu");
        }
    });
});