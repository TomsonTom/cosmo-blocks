/*
 * Pushable Component
 * com/pushable
 *
 * NO UNIT TEST
 *
 * This component allows entity to be pushed.
 */

define([], function()
{
    TypeOfMove = {SimplePush:0,PushUp:1};
    CurrentMove = {Up:0,Dir:1}; // Only for push-up

    Crafty.c("Pushable",
    {
        init: function()
        {
            this.requires("2D");
            this.framesRemaining = 0;
            this.speed = 0;
            this.typeOfMove = TypeOfMove.SimplePush;
            this.currentMove = undefined;

            this.bind("EnterFrame", this._performPush);
        },

        // Attempt to push the entity
        // Returns false if the entity couldn't be pushed
        // Returns true  if the entity was pushed
        tryPush: function(dir)
        {
            // We won't move an entity if it's already being pushed
            if (this.framesRemaining > 0)
                return false;

            var e = this;
            var pos = this.pos();
            pos._x += dir * 70;

            var entitiesInDir = Crafty.map.search(pos); // Get entities in given direction next to this entity
            var blockingBlocks = []; // Blocks that blocks the way

            _.forEach(entitiesInDir, function(ent)
            {
                if (ent.has("block") || ent.has("fence"))
                    blockingBlocks.push(ent);
            });

            // We won't move left/right, because there's another block blocking the way
            if (blockingBlocks.length > 0)
            {
                // But we may still be able to move the block up and then right/left
                pos._y -= 70; // Move a row up
                pos._x -= dir * 70; // pos._y = directly above the player
                pos._w *= 2; // We're checking for 2 free places, not only one

                // If we try to push left, we must adjust the x
                if (dir < 0)
                    pos._x -= 70;

                entitiesInDir = Crafty.map.search(pos);
                blockingBlocks.length = 0;

                _.forEach(entitiesInDir, function(ent)
                {
                    if (ent.has("block") || ent.has("fence"))
                        blockingBlocks.push(ent);
                });

                // We can't even move up :(
                if (blockingBlocks.length > 0)
                    return false;
                else
                {
                    this.typeOfMove = TypeOfMove.PushUp;
                    this.currentMove = CurrentMove.Up;
                    this.speed = dir * 5;
                    this.framesRemaining = 30; // This number was found out experimentally to ensure smooth animation
                }
            }
            else
            {
                // Set up entity to be moved smoothly
                this.typeOfMove = TypeOfMove.SimplePush;
                this.speed = dir * 5;
                this.framesRemaining = 14;
            }

            return true;
        },

        _performPush: function()
        {
            if (this.framesRemaining > 0 && this.speed !== 0)
            {
                if(this.framesRemaining === 1)
                    this.trigger("PushEnd");

                if (this.typeOfMove === TypeOfMove.SimplePush)
                {
                    this.x += this.speed;
                    this.framesRemaining--;
                }
                else if (this.typeOfMove === TypeOfMove.PushUp)
                {
                    if (this.currentMove === CurrentMove.Up)
                    {
                        this.y -= 7; // This number was found out experimentally to ensure smooth animation
                        this.framesRemaining--;
                        if (this.framesRemaining < 15)
                        {
                            this.framesRemaining = 14;
                            this.currentMove = CurrentMove.Dir;
                        }
                    }
                    else
                    {
                        this.x += this.speed;
                        this.framesRemaining--;
                    }
                }
            }
        }
    });
});