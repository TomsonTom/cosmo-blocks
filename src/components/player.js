/*
 * Player Component
 * com/player
 *
 * NO UNIT TEST
 */

define(["com/facing", "com/gravityplus", "com/twowaydouble", "com/pushblocks"], function()
{
    Crafty.c("Player",
    {
        init: function()
        {
            this.requires("2D, Facing, GravityPlus, TwowayDouble, Collision, SpriteAnimation, PushBlocks");
            this.addComponent("char_walk_01"); // Add sprite
            this.collision([0,0], [20,0], [20, 20], [0, 20]);


            // Set up componenets
            this.twoway(6, 10);
            this.gravity("solid", 15);
            this.gravityConst(0.6);
            this.facing(FacingDirection.RIGHT);

            // Set up animation
            this.animate("PlayerRunning", 1, 0, 8);

            this.x = 100;
            this.y = 300;
            this.z = 999; // The player is on top everything except GUI (1000)

            this.bind("Moved", this._moved);
            this.bind("EnterFrame", this._onEnterFrame);
            this.bind("NewDirection", this._newDirection);

            this._oldFalling = false;
            this._oldX = this.x;
        },

        // !! This won't get triggered when 'y' coordinate changes!
        //    "EnterFrame" is used to detect jumping.
        _moved: function(old)
        {
            // Restrict player to the screen area
                 if (this.x > (840-this.w+5))
                     this.x = old.x;
            else if (this.x < -5)
                     this.x = old.x;

            // Check collision with blocks
            var player = this;
            var blocks = CraftyFinder.Find("block");
            var pos = this.pos();
            pos.x = pos._x + 15; pos.y = pos._y;
            pos.w = pos._w - 30; pos.h = pos._h;

            _.forEach(blocks, function(block)
            {
                if (block.intersect(pos))
                    player.x = old.x;
            });
        },

        _onEnterFrame: function()
        {
            // When the player starts or stops jumping
            if (this._falling !== this._oldFalling)
            {
                this.stop();
                this.toggleComponent("char_walk_01, char_jump");

                // If the player stops jumping, we have to trigger NewDirection manually
                // so the animation can be resetted (if the player moves left/right during jump)
                if (!this._falling)
                {
                    this.trigger("NewDirection", {x:(this.x-this._oldX)});
                }
            }

            this._oldFalling = this._falling;
            this._oldX = this.x;
        },

        // _onPlayerHitsObject: function(e)
        // {
        //     Logger.Log("hit");

        //     var pos = this.pos();
        //     pos._y -= 5;

        //     if (e.has("block") && e.intersect(pos))
        //         Logger.Log("HIIIIT");
        // },

        _newDirection: function(dir)
        {
            // Player starts moving right
            if (dir.x > 0)
            {
                this.face(FacingDirection.RIGHT);
                if (!this._falling)
                    this.animate("PlayerRunning", 2, -1);
            }
            // Player starts moving left
            else if (dir.x < 0)
            {
                this.face(FacingDirection.LEFT);
                if (!this._falling)
                    this.animate("PlayerRunning", 2, -1);
            }
            // Player stops moving
            else if (dir.x === 0 && !this._falling)
            {
                this.x++; this.x--; // This line is necessary due to a bug in CraftyJS
                this.reset();
            }
        }
    });
});