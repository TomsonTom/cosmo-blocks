/*
 * Combination Check Launcher Component
 * com/combichecklauncher
 *
 * NO UNIT TEST
 */

define(["util/combinationchecker"], function()
{
    Crafty.c("CombiCheckLauncher",
    {
        init: function()
        {
            this.requires("Delay");
            this.delay(this.check, 250);
            this.challengeMode = false;
        },

        setChallenge: function(challenge)
        {
            this.challengeMode = challenge;
        },

        check: function()
        {
            CombinationChecker.Check();
            if (this.challengeMode === true)
                CombinationChecker.CheckGameOver();
            this.delay(this.check, 250);
        }
    });
});