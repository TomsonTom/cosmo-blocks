/*
 * Block Spawner Component
 * com/blockspawner
 *
 * NO UNIT TEST
 */

define(["com/pushable", "util/combinationchecker"], function()
{
    Crafty.c("BlockSpawner",
    {
        init: function()
        {
            this.requires("Delay");

            this.spawnDelay = 2000;
            this.shortenDelay = false;

            this.delay(this.spawnBlock, 100);
            this.delay(this._shortenDelay, 10000);
        },

        allowShortenDelay: function(allowShorten)
        {
            this.shortenDelay = allowShorten;
        },

        spawnBlock: function()
        {
            var y = -100;
            var x = 70 + Math.floor(Math.random()*10)*70;
            if (x > 700) x = 700;

            var colour = this._getRandomColour();

            var block = Crafty.e("2D, Canvas, GravityPlus, Pushable, solid, block, block_" + colour);
            block.attr({ x: x, y: y, z: 200, blockColour: colour });
            block.gravity("solid", 15);
            //block.bind("Change", function(){CombinationChecker.Check();}); // This was an FPS drain
                                                                             // replaced by com/CombiCheckLauncher


            this.delay(this.spawnBlock, this.spawnDelay);
        },

        _shortenDelay: function()
        {
            if (this.shortenDelay === true)
            {
                if (this.spawnDelay > 500)
                    this.spawnDelay -= 100;
            }

            this.delay(this._shortenDelay, 10000);
        },

        _getRandomColour: function()
        {
            var colours = ['yellow', 'red', 'green', 'blue', 'blue'];
            var id = Math.floor(Math.random()*4);
            return colours[id];
        }
    });
});