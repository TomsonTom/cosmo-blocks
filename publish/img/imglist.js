var sprites =
{
    "char_walk":
    {
        path: "img/character/walksheet.png",
        tile_width: 75,
        tile_height: 96,
        padding_x: 0,
        padding_y: 0,
        elements:
        {
            "char_walk_01": [0, 0],
            "char_walk_02": [1, 0],
            "char_walk_03": [2, 0],
            "char_walk_04": [3, 0],
            "char_walk_05": [4, 0],
            "char_walk_06": [5, 0],
            "char_walk_07": [6, 0],
            "char_walk_08": [7, 0],
            "char_walk_09": [8, 0],
            "char_walk_10": [9, 0],
            "char_walk_11": [10, 0]
        }
    },
    "char_jump":
    {
        path: "img/character/jump.png"
    },
    "ground_grass":
    {
        path: "img/environment/ground.png"
    },
    "vegetation":
    {
        path: "img/environment/vegetation.png",
        tile_width: 1,
        tile_height: 1,
        padding_x: 0,
        padding_y: 0,
        elements:
        {
            "hill_long": [0, 0, 48, 146],
            "hill_short": [49, 0, 48, 146],
            "rock": [97, 0, 70, 146],
            "grass": [167, 0, 34, 146],
            "mushroom": [201, 0, 39, 146]
        }
    },
    "fences":
    {
        path: "img/environment/fences.png",
        tile_width: 70,
        tile_height: 61,
        padding_x: 0,
        padding_y: 0,
        elements:
        {
            "fence_broken": [0, 0],
            "fence_new": [1, 0]
        }
    },
    "clouds":
    {
        path: "img/environment/clouds.png",
        tile_width: 1,
        tile_height: 1,
        padding_x: 0,
        padding_y: 0,
        elements:
        {
            "cloud_1": [0, 0, 129, 63],
            "cloud_2": [129, 0, 128, 63],
            "cloud_3": [257, 0, 128, 63]
        }
    },
    "blocks_colour":
    {
        path: "img/blocks/colours.png",
        tile_width: 70,
        tile_height: 70,
        padding_x: 0,
        padding_y: 0,
        elements:
        {
            "block_yellow": [0, 0],
            "block_red": [1, 0],
            "block_green": [0, 1],
            "block_blue": [1, 1]
        }
    }
};